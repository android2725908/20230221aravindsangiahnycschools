package com.aravind.androidchallenge.nycschools.view;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.aravind.androidchallenge.nycschools.R;
import com.aravind.androidchallenge.nycschools.databinding.FragmentBodyBinding;

public class BodyFragment extends BaseFragment {
    FragmentBodyBinding mBinding;

    // When Fragment view is created, load layout file
    // Bind view with ViewModel
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_body, container, false);
        mBinding.setViewModel(mViewModel);
        return mBinding.getRoot();
    }

}
