package com.aravind.androidchallenge.nycschools.viewmodel;

import com.aravind.androidchallenge.nycschools.model.ApiSchool;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {ApiModule.class})
interface ApiComponent {
    ApiSchool provideApi();

    void inject(DataViewModel viewModel);
}