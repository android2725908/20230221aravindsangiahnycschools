package com.aravind.androidchallenge.nycschools.model;

public class SchoolDetail extends SchoolSimple {
    protected String fax_number;
    protected String city;
    protected String state_code;
    protected String zip;
    protected String location;
    protected String website;
    protected String subway;
    protected String bus;
    protected String total_students;
    protected String overview_paragraph;

    public SchoolDetail(String dbn, String school_name, String location, String zip, String website, String state_code, String subway, String fax_number, String city, String total_students, String bus, String overview_paragraph) {
        this.dbn = dbn;
        this.school_name = school_name;
        this.location = location;
        this.zip = zip;
        this.website = website;
        this.state_code = state_code;
        this.subway = subway;
        this.fax_number = fax_number;
        this.city = city;
        this.total_students = total_students;
        this.bus = bus;
        this.overview_paragraph = overview_paragraph;
    }

    public String getLocation() {
        return location;
    }

    public String getZip() {
        return zip;
    }

    public String getWebsite() {
        return website;
    }

    public String getState_code() {
        return state_code;
    }

    public String getSubway() {
        return subway;
    }

    public String getFax_number() {
        return fax_number;
    }

    public String getCity() {
        return city;
    }

    public String getTotal_students() {
        return total_students;
    }

    public String getBus() {
        return bus;
    }

    public String getOverview_paragraph() {
        return overview_paragraph;
    }
}

