package com.aravind.androidchallenge.nycschools.model;

import com.aravind.androidchallenge.nycschools.Util;

public class SchoolSimple {
    protected String dbn;
    protected String school_name;
    protected String phone_number;
    protected String school_email;

    public SchoolSimple() { }

    public SchoolSimple(String dbn, String school_name, String phone_number, String school_email) {
        this.dbn = dbn;
        this.school_name = school_name;
        this.phone_number = phone_number;
        this.school_email = school_email;
    }

    public String getDbn() {
        return dbn;
    }

    public String getSchool_name() {
        return school_name;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public String getSchool_email() {
        return school_email;
    }

    public boolean searchName(String strSearch) {
        return Util.isIncluded(school_name, strSearch);
    }
}

