package com.aravind.androidchallenge.nycschools;

import android.app.Application;
import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.FragmentActivity;

import com.aravind.androidchallenge.nycschools.viewmodel.DataViewModel;

public class NycSchoolsApplication extends Application {
    static DataViewModel mViewModel;

    public static DataViewModel getViewModel(FragmentActivity activity) {
        if( mViewModel == null )
            mViewModel = ViewModelProviders.of(activity).get(DataViewModel.class);
        return mViewModel;
    }

}
