package com.aravind.androidchallenge.nycschools.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.aravind.androidchallenge.nycschools.NycSchoolsApplication;
import com.aravind.androidchallenge.nycschools.viewmodel.DataViewModel;

public class BaseActivity extends AppCompatActivity {
    public DataViewModel mViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = NycSchoolsApplication.getViewModel(this);
    }

}
