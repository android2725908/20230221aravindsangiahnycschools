package com.aravind.androidchallenge.nycschools;

public class Util {

    public static boolean isIncluded(String strText, String strSearch) {
        if( strText == null || strText.length() < 1 )
            return false;

        strSearch = strSearch.trim();
        if( strSearch.length() < 2 )
            return true;

        strSearch = strSearch.toLowerCase();
        strText = strText.toLowerCase();

        if( strText.indexOf(strSearch) >= 0 )
            return true;

        return false;
    }
}
