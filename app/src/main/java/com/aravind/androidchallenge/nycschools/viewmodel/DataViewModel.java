package com.aravind.androidchallenge.nycschools.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;


import com.aravind.androidchallenge.nycschools.model.ApiSchool;
import com.aravind.androidchallenge.nycschools.model.SchoolDetail;
import com.aravind.androidchallenge.nycschools.model.SchoolScore;
import com.aravind.androidchallenge.nycschools.model.SchoolSimple;

import java.util.ArrayList;

import javax.inject.Inject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataViewModel extends ViewModel {
    private MutableLiveData<ArrayList<SchoolSimple>> mListSchools;
    private MutableLiveData<ArrayList<SchoolSimple>> mTotalSchools;
    private MutableLiveData<SchoolSimple> mSchoolSimple;
    private MutableLiveData<SchoolScore> mSchoolScore;
    private MutableLiveData<SchoolDetail> mSchoolDetail;

    @Inject
    ApiSchool mApi;

    public DataViewModel() {
        ApiComponent component = DaggerApiComponent.builder().build();
        component.inject(this);
    }

    public MutableLiveData<ArrayList<SchoolSimple>> getListSchools() {
        if (mListSchools == null) {
            mListSchools = new MutableLiveData<ArrayList<SchoolSimple>>();
        }
        return mListSchools;
    }

    public MutableLiveData<ArrayList<SchoolSimple>> getTotalSchools() {
        if (mTotalSchools == null) {
            mTotalSchools = new MutableLiveData<ArrayList<SchoolSimple>>();
        }
        return mTotalSchools;
    }

    public MutableLiveData<SchoolSimple> getSchoolSimple() {
        if (mSchoolSimple == null) {
            mSchoolSimple = new MutableLiveData<SchoolSimple>();
        }
        return mSchoolSimple;
    }

    public MutableLiveData<SchoolScore> getSchoolScore() {
        if (mSchoolScore == null) {
            mSchoolScore = new MutableLiveData<SchoolScore>();
        }
        return mSchoolScore;
    }

    public MutableLiveData<SchoolDetail> getSchoolDetail() {
        if (mSchoolDetail == null) {
            mSchoolDetail = new MutableLiveData<SchoolDetail>();
        }
        return mSchoolDetail;
    }

    public void reqSchoolList() {
        Call<ArrayList<SchoolSimple>> call = mApi.getSchoolList();
        call.enqueue(new Callback<ArrayList<SchoolSimple>>() {

            @Override
            public void onResponse(Call<ArrayList<SchoolSimple>> call, Response<ArrayList<SchoolSimple>> response) {
                ArrayList<SchoolSimple> totalSchools = response.body();
                getTotalSchools().setValue(totalSchools);

                ArrayList<SchoolSimple> listSchools = new ArrayList<SchoolSimple>();
                for( SchoolSimple schoolSimple : totalSchools) {
                    listSchools.add(schoolSimple);
                }
                getListSchools().setValue(listSchools);

                if( listSchools.size() > 0 ) {
                    reqSchoolScore(listSchools.get(0));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SchoolSimple>> call, Throwable t) {}
        });
    }

    // Request school data to server
    public void reqSchoolDetail(String dbn) {
        Call<ArrayList<SchoolDetail>> call = mApi.getSchoolDetail(dbn);
        call.enqueue(new Callback<ArrayList<SchoolDetail>>() {

            @Override
            public void onResponse(Call<ArrayList<SchoolDetail>> call, Response<ArrayList<SchoolDetail>> response) {
                ArrayList<SchoolDetail> listDetails = response.body();
                int size = listDetails.size();
                if( size > 0 ) {
                    SchoolDetail detail = listDetails.get(0);
                    getSchoolDetail().setValue(detail);
                }
                else {
                    SchoolSimple schoolSimple = getSchoolSimple().getValue();
                    SchoolDetail detail = new SchoolDetail(schoolSimple.getDbn(),
                            schoolSimple.getSchool_name(), "", "", "", "", "", "", "", "", "", "");
                    getSchoolDetail().setValue(detail);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SchoolDetail>> call, Throwable t) {}
        });
    }

    // Request SAT score data to server
    public void reqSchoolScore(String dbn) {
        Call<ArrayList<SchoolScore>> call = mApi.getSchoolScore(dbn);
        call.enqueue(new Callback<ArrayList<SchoolScore>>() {

            @Override
            public void onResponse(Call<ArrayList<SchoolScore>> call, Response<ArrayList<SchoolScore>> response) {
                ArrayList<SchoolScore> listGrades = response.body();
                int size = listGrades.size();
                if( size > 0 ) {
                    SchoolScore score = listGrades.get(0);
                    getSchoolScore().setValue(score);
                }
                else {
                    SchoolSimple schoolSimple = getSchoolSimple().getValue();
                    SchoolScore score = new SchoolScore(schoolSimple.getDbn(),
                            schoolSimple.getSchool_name(), "0", "", "", "");
                    getSchoolScore().setValue(score);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<SchoolScore>> call, Throwable t) {}
        });
    }

    // Request particular school detail information to server.
    public void reqSchoolScore(int schoolIndex) {
        ArrayList<SchoolSimple> schoolSimples = getListSchools().getValue();
        if( schoolSimples == null || schoolSimples.size() <= schoolIndex )
            return;
        SchoolSimple schoolSimple = schoolSimples.get(schoolIndex);
        reqSchoolScore(schoolSimple);
    }

    // Request particular school detail information to server.
    public void reqSchoolScore(SchoolSimple schoolSimple) {
        getSchoolSimple().setValue(schoolSimple);
        reqSchoolScore(schoolSimple.getDbn());
        reqSchoolDetail(schoolSimple.getDbn());
    }

    // Text changing listener of Search EditText
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        String strSearch = s.toString();
        if( strSearch.length() < 2 )
            return;
        ArrayList<SchoolSimple> listSchools = getSearchedSchoolList(strSearch);
        getListSchools().setValue(listSchools);
    }

    // Make a ArrayList by searching School name in Total School list
    protected ArrayList<SchoolSimple> getSearchedSchoolList(String strSearch) {
        ArrayList<SchoolSimple> listSchools = new ArrayList<SchoolSimple>();
        for(SchoolSimple schoolSimple : getTotalSchools().getValue()) {
            if( schoolSimple.searchName(strSearch) )
                listSchools.add(schoolSimple);
        }
        return listSchools;
    }

}
