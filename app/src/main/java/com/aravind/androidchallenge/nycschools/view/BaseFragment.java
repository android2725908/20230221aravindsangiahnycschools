package com.aravind.androidchallenge.nycschools.view;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.aravind.androidchallenge.nycschools.NycSchoolsApplication;
import com.aravind.androidchallenge.nycschools.viewmodel.DataViewModel;

public class BaseFragment extends Fragment {
    DataViewModel mViewModel;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewModel = NycSchoolsApplication.getViewModel(getActivity());
    }

}