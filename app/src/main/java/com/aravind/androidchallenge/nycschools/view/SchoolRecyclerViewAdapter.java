package com.aravind.androidchallenge.nycschools.view;

import android.arch.lifecycle.Observer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;

import com.aravind.androidchallenge.nycschools.R;
import com.aravind.androidchallenge.nycschools.databinding.SchoolItemBinding;
import com.aravind.androidchallenge.nycschools.model.SchoolSimple;
import com.aravind.androidchallenge.nycschools.viewmodel.DataViewModel;

import java.util.ArrayList;

public class SchoolRecyclerViewAdapter extends RecyclerView.Adapter<SchoolRecyclerViewAdapter.SchoolVH> {
    private DataViewModel mViewModel;
    Fragment mParent;
    RecyclerView.Adapter<SchoolVH> mAdapter;

    public SchoolRecyclerViewAdapter(DataViewModel viewModel, Fragment parent) {
        this.mViewModel = viewModel;
        this.mParent = parent;
        mAdapter = this;

        // make School simple data list Observer object
        final Observer<ArrayList<SchoolSimple>> schoolsObserver = new Observer<ArrayList<SchoolSimple>>() {
            @Override
            public void onChanged(@Nullable final ArrayList<SchoolSimple> schools) {
                // When School simple data list is changed update RecyclerView
                mAdapter.notifyDataSetChanged();
            }
        };
        viewModel.getListSchools().observe(this.mParent, schoolsObserver);
    }

    // Return list items count
    @Override
    public int getItemCount() {
        ArrayList<SchoolSimple> schools = mViewModel.getListSchools().getValue();
        if( schools == null )
            return 0;
        return schools.size();
    }

    // Make ViewHolder & View binding object
    @Override
    public SchoolVH onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        // Get the view binding object of custom list item layout
        LayoutInflater inflater = LayoutInflater.from(mParent.getContext());
        SchoolItemBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.school_item, viewGroup, false);
        binding.setViewModel(mViewModel);
        binding.setListFragment((ListFragment)mParent);

        return new SchoolVH(binding);
    }

    // When ViewHolder is binded set data to binding object
    @Override
    public void onBindViewHolder(SchoolVH viewHolder, int position) {
        viewHolder.bind(position);
    }

    // Reuse views
    public static class SchoolVH extends RecyclerView.ViewHolder {
        public SchoolItemBinding binding;

        public SchoolVH(SchoolItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(int index) {
            binding.setIndex(index);
            binding.executePendingBindings();
        }
    }
}

