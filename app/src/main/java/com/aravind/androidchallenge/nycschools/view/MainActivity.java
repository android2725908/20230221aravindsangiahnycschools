package com.aravind.androidchallenge.nycschools.view;

import android.os.Bundle;

import com.aravind.androidchallenge.nycschools.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

}