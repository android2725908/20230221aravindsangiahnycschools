package com.aravind.androidchallenge.nycschools.view;

import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import com.aravind.androidchallenge.nycschools.R;
import com.aravind.androidchallenge.nycschools.databinding.FragmentListBinding;

public class ListFragment extends BaseFragment {
    boolean mMultiPanel = false;

    // When Fragment view is created, load layout file
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup
            container, Bundle savedInstanceState) {
        mViewModel.reqSchoolList();
        FragmentListBinding binding = DataBindingUtil.inflate(
                inflater, R.layout.fragment_list, container, false);
        binding.setViewModel(mViewModel);
        initList(binding);

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initBodyFragment();
    }

    private void initBodyFragment() {
        BodyFragment bodyF = (BodyFragment)getActivity().getSupportFragmentManager().findFragmentById(R.id.fragBody);
        if( bodyF != null && bodyF.getView().getVisibility() == View.VISIBLE ) {
            mMultiPanel = true;
        }
    }

    // Init RecyclerView adapter & Request School list to server
    protected void initList(FragmentListBinding binding) {
        SchoolRecyclerViewAdapter rvAdapter = new SchoolRecyclerViewAdapter(mViewModel, this);
        binding.rvSchool.setAdapter( rvAdapter );
        binding.rvSchool.setLayoutManager(new LinearLayoutManager(getContext(),
                LinearLayoutManager.VERTICAL, false));

        binding.rvSchool.addItemDecoration(new DividerItemDecoration(getContext(), 1));
    }

    // School RecyclerView item selection event
    public void onSchoolSelected(int position) {
        // When single panel mode, switch to 2nd fragment
        if( mMultiPanel == false )
            switch2BodyFragment();

        mViewModel.reqSchoolScore(position);
    }

    private void switch2BodyFragment() {
        BodyFragment bodyF = new BodyFragment();

        getActivity().getSupportFragmentManager().beginTransaction()
                .replace(R.id.panelList, bodyF)
                .addToBackStack(null)
                .commit();
    }
}

